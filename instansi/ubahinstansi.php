<?php
	session_start();
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
	include('../../config.inc');
?>
<!DOCTYPE html>
<html>
<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
		<script>
		function addRow(tableID) {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			if(rowCount < 3){							// limit the user from creating fields more than your limits
				var row = table.insertRow(rowCount);
				var colCount = table.rows[0].cells.length;
				for(var i=0; i<colCount; i++) {
					var newcell = row.insertCell(i);	
					newcell.innerHTML = table.rows[0].cells[i].innerHTML;
				}
				}else{
		 		alert("Maximum Other Number Phone is 3.");
			   
			}
		}

		function deleteRow(tableID) {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 1) { 						// limit the user from removing all the fields
					alert("Cannot Remove All The Phone.");
					break;
				}
			table.deleteRow(i);
			rowCount--;
			i--;
		}
	}

}
</script>

	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="../pengguna/pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="#" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<h2>Edit Entry</h2>
							<form action="" method="post" enctype="multipart/form-data">
							<div style="margin-left:40px">
								<?php
								$id = $_GET['id'];
								$result = pg_query("select * from sinodar.instance where id='$id'");
								$arr = pg_fetch_array($result);?>
									<input type="hidden" name="id" value="<?php echo $arr['id'] ?>">
								<div class="row" style="margin-top:15px">
									<label><b>Instance Name *</b></label>
									<input type="text" name="name" pattern="[a-zA-Z\0-9\s-\/,/.]*" value="<?php echo $arr['name'] ?>" required>
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Category *</b></label>
									<input type="radio" name="instance_type" value="hospital" checked> Hospital
									<input type="radio" name="instance_type" value="police station" style="margin-left:10px;"> Police station
									<input type="radio" name="instance_type" value="fire station" style="margin-left:10px;"> Fire station
								</div>
								<div class="row" style="margin-top:20px">
									<label><b>Address *</b></label>
									<input type="text" name="address" value="<?php echo $arr['address'] ?>" pattern="[A-Za-z0-9\s-\/,/.]*">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Longitude *</b></label>
									<input type="text" id="longitude" name="longitude" value="<?php echo rad2deg($arr['longitude']) ?>">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Latitude *</b></label>
									<input type="text" id="latitude" name="latitude" value="<?php echo rad2deg($arr['latitude']) ?>">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Location</b></label>
									<div id="map_canvas"></div>
									<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = -6.389236;
										var defaultLng = 106.829363;

											// creates the map
											// zooms
											// centers the map
											// sets the map?s type
											var map = new google.maps.Map(document.getElementById('map_canvas'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});


											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
										
											// centers the map on markers coords
											map.setCenter(myMarker.position);

											// adds the marker on the map
											myMarker.setMap(map);
									</script>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Phone *</b></label>
									<input type="text" name="primary_phone_num" value="<?php echo $arr['primary_phone_num'] ?>" pattern="[0-9]{8,100}" required>
								</div>
								<div class="row" style="margin-top:15px"> 
									<input type="button" value="Add Phone Num" onClick="addRow('dataTable')" /> 
									<input type="button" value="Remove Phone Num" onClick="deleteRow('dataTable')"  /> 
								</div>
								<?php $resultNumber = pg_query("select phone_num from sinodar.instance_other_number where id='".$arr['id']."'");
									if(pg_num_rows($resultNumber)==0) 
									{  
										echo '<div class="row" style="margin-top:15px">';
											echo " <table id = 'dataTable'> ";
												echo '<td><input type="checkbox" name="chk[]" checked="checked" /></td>';
												echo '<td>';
													echo '<label><b>Other Phone Num</b></label>';
													echo '<input type="text" name="otherNum[]" pattern="[0-9]{8,100}" />';
												echo '</td>';
											echo '</table>';
										echo '</div>';
									 } 
									 else{
										
									 
												
												echo '<div class="row" style="margin-top:15px">';
												echo "<table id = 'dataTable'>";
												$counter = 0;
												while($nums = pg_fetch_array($resultNumber)){
												$number = $nums['phone_num'];
												echo '<tr>';
												echo '<td><input type="checkbox" name="chk[]" checked="checked" /></td>';
												echo'<td>';
													echo'<label><b>Other Phone Num</b></label>';
													echo'<input type="text" name="otherNum[]" value="';
													echo $number;
													echo '" />';
												echo '</td>';
												echo '</tr>';
												}
												echo '</table>';
												echo '</div>';
											
									 	
																	
									} 
								?>
								<div class="row" style="margin-top:15px">
									<label><b>Description</b></label>
									<textarea rows="4" cols="50" name="description" value="<?php echo $arr['description'] ?>"></textarea>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Image</b></label>
									<input type="file" name="file">
								</div>
								<div class="row" style="margin-top:15px" align="center">
									<input class="btn" type="submit" name="submit" value="Submit">
								</div>
							</div>
							</form>
								<?php
								$picDir = "../../instance_pics/";
								$saveDir = '../../instance_pics/';
								$timestamp  = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
								$dbPicDir = 'http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/instance_pics/';
								$maxAllowedSize = 5 * 1024 * 1024; // in byte
								$allowedExts = array("gif", "jpeg", "jpg", "png");
								$allowedType = array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png");
								$extension = end(explode(".", $_FILES["file"]["name"]));
								if(in_array($_FILES["file"]["type"], $allowedType) && 
	  							 in_array($extension, $allowedExts) &&
	  							 $_FILES["file"]["size"] <= $maxAllowedSize) {
									if($_FILES["file"]["error"] > 0) {
										echo "<h1>Error: " . $_FILES["file"]["error"] . "</h1>";
										} else {
										$filename = str_replace(array(" ","-", ":"), "_", $timestamp . '_' . $_FILES["file"]["name"]);
										$pictureURL = $saveDir . $filename;
										$upFile = $picDir . date('Y_m_d_H_i_s') . $_FILES['file']['name'];
										if(!is_null($pictureURL)){ move_uploaded_file($_FILES["picture"]["tmp_name"], $saveDir . $filename);}
										if(!move_uploaded_file($_FILES["file"]["tmp_name"], $pictureURL)) {
											echo 'Problem could not move file to destination. Please check again later.';
											exit;
										}
								}
								} else {
								echo "<h3> </h3>";
								}
											
								if(isset($_POST['submit']))
								{
								$latitude = deg2rad($_POST['latitude']);
								$longitude = deg2rad($_POST['longitude']);
								$timestamp  = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
								$resultPic = pg_query("select picture_url from sinodar.instance where id='".$_POST['id']."'");
								$arrs = pg_fetch_array($resultPic);
								$prev_url = $arrs['picture_url'];
								$previ_url = str_replace('http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/instance_pics/','/home/fasilkom/mahasiswa/r/rifky.fakhrul/sinodar/instance_pics/',$prev_url);
								$otherNumber = $_POST['otherNum'];
								
								$upFile2 = str_replace('../../','http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/',$pictureURL);
								if(!empty($previ_url)&&!empty($upFile2))
								{
									if(!empty($prev_url))
									{
									unlink($previ_url);
									$hasil = pg_query("update sinodar.instance set name='".$_POST['name']."', address='".$_POST['address']."', longitude='$longitude', latitude='$latitude', primary_phone_num='".$_POST['primary_phone_num']."', instance_type='".$_POST['instance_type']."', description='".$_POST['description']."', picture_url='$upFile2', last_edit_timestamp='$timestamp' where id='".$_POST['id']."'");
									if($hasil)
									{
										$resultID = pg_query("select id from sinodar.instance where name='".$_POST['name']."' and address='".$_POST['address']."'");
										$deletePhone = pg_query("delete from sinodar.instance_other_number where id = '".$_POST['id']."'");
										if($resultID)
										{
											$arr = pg_fetch_array($resultID);
											$id = $arr['id'];
											foreach($otherNumber as $a => $b)
											{
												if(!empty($otherNumber[$a]) || $otherNumber[$a]!=null )
												{
													$resultOtherNumber = pg_query("insert into sinodar.instance_other_number(id,phone_num) values('".$id."',$otherNumber[$a])");
												}
											}
										}
										echo "<script type='text/javascript'>alert('Entry successfully edited');window.location.assign('instansi.php');</script>";
									}
									else
									{
										echo "<script type='text/javascript'>alert('Edit entry fail');window.location.assign('instansi.php');</script>";
									}
									}
															
								}
								else if(empty($previ_url)&&!empty($upFile2))
								{
									$hasil = pg_query("update sinodar.instance set name='".$_POST['name']."', address='".$_POST['address']."', longitude='$longitude', latitude='$latitude', primary_phone_num='".$_POST['primary_phone_num']."', instance_type='".$_POST['instance_type']."', description='".$_POST['description']."', picture_url='$upFile2', last_edit_timestamp='$timestamp' where id='".$_POST['id']."'");
									if($hasil)
									{
										$resultID = pg_query("select id from sinodar.instance where name='".$_POST['name']."' and address='".$_POST['address']."'");
										$deletePhone = pg_query("delete from sinodar.instance_other_number where id = '".$_POST['id']."'");
										if($resultID)
										{
											$arr = pg_fetch_array($resultID);
											$id = $arr['id'];
											foreach($otherNumber as $a => $b)
											{
												if(!empty($otherNumber[$a]) || $otherNumber[$a]!=null )
												{
													$resultOtherNumber = pg_query("insert into sinodar.instance_other_number(id,phone_num) values('".$id."',$otherNumber[$a])");
												}
											}
										}
										echo "<script type='text/javascript'>alert('Entry successfully edited');window.location.assign('instansi.php');</script>";
									}
									else
									{
										echo "<script type='text/javascript'>alert('Edit entry fail');window.location.assign('instansi.php');</script>";
									}
								}
								else if(!empty($previ_url)&&empty($upFile2))
								{
									$hasil = pg_query("update sinodar.instance set name='".$_POST['name']."', address='".$_POST['address']."', longitude='$longitude', latitude='$latitude', primary_phone_num='".$_POST['primary_phone_num']."', instance_type='".$_POST['instance_type']."', description='".$_POST['description']."', picture_url='$prev_url', last_edit_timestamp='$timestamp' where id='".$_POST['id']."'");
									if($hasil)
									{
										$resultID = pg_query("select id from sinodar.instance where name='".$_POST['name']."' and address='".$_POST['address']."'");
										$deletePhone = pg_query("delete from sinodar.instance_other_number where id = '".$_POST['id']."'");
										if($resultID)
										{
											$arr = pg_fetch_array($resultID);
											$id = $arr['id'];
											foreach($otherNumber as $a => $b)
											{
												if(!empty($otherNumber[$a]) || $otherNumber[$a]!=null )
												{
													$resultOtherNumber = pg_query("insert into sinodar.instance_other_number(id,phone_num) values('".$id."',$otherNumber[$a])");
												}
											}
										}
										echo "<script type='text/javascript'>alert('Entry successfully edited');window.location.assign('instansi.php');</script>";
									}
									else
									{
										echo "<script type='text/javascript'>alert('Edit entry fail');window.location.assign('instansi.php');</script>";
									}
								}
								else
								{
									$hasil2 = pg_query("update sinodar.instance set name='".$_POST['name']."', address='".$_POST['address']."', longitude='$longitude', latitude='$latitude', primary_phone_num='".$_POST['primary_phone_num']."', instance_type='".$_POST['instance_type']."', description='".$_POST['description']."', picture_url=NULL, last_edit_timestamp='$timestamp' where id='".$_POST['id']."'");
									if($hasil2)
									{
										$resultID = pg_query("select id from sinodar.instance where name='".$_POST['name']."' and address='".$_POST['address']."'");
										$deletePhone = pg_query("delete from sinodar.instance_other_number where id = '".$_POST['id']."'");
										if($resultID)
										{
											$arr = pg_fetch_array($resultID);
											$id = $arr['id'];
											foreach($otherNumber as $a => $b)
											{
												if(!empty($otherNumber[$a]) || $otherNumber[$a]!=null )
												{
													$resultOtherNumber = pg_query("insert into sinodar.instance_other_number(id,phone_num) values('".$id."',$otherNumber[$a])");
												}
											}
										}
										echo "<script type='text/javascript'>alert('Entry successfully edited');window.location.assign('instansi.php');</script>";
									}
									else
									{
										echo "<script type='text/javascript'>alert('Edit entry fail');window.location.assign('instansi.php');</script>";
									}
								}
								}
										
									  
							?>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>

</html>
