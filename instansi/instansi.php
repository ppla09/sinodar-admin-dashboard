<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<link href="../css/jquery-ui-1.10.4.custom.css" rel="stylesheet">
		<script src="../js/jquery-1.10.2.js"></script>
		<script src="../js/jquery-ui-1.10.4.custom.js"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
		<script>$(function(){$("#tabs").tabs();});</script>

	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="../pengguna/pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasiedit/konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<div>
							<h2>Instance List</h2>
							</div>
							<div>
							<a class="btn" role="button" href="tambahinstansi.php">Create new entry</a>
							</div>
							<!--<div style="margin-top:10px">
								Category
								<select id="instansi">
								<option value="all">All</option>
								<option value="hospital">Hospital</option>
								<option value="police station">Police Station</option>
								<option value="fire station">Fire Station</option>
								</select>
								<!--
								<label id="coba" for="instansi">I will change if you change dropdown menu value</label>
								<script type="text/javascript">
									var dropdown = document.getElementById('instansi');
									var cek = document.getElementById('coba');
									dropdown.onchange = function(){
									cek.innerHTML = this.options[this.selectedIndex].value;
									};
								</script>
								
							</div>-->
							<div style="margin-top:10px">
								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Rumah Sakit</a></li>
										<li><a href="#tabs-2">Kantor Polisi</a></li>
										<li><a href="#tabs-3">Pemadam Kebakaran</a></li>
									</ul>
										<div id="tabs-1">
										<table class="table table-bordered">
											<tr>
												<th>ID</th>
												<th>Instance Name</th>
												<th width="90px">Action</th>
											</tr>
											<?php
											$hitung = pg_query("select count(id) from sinodar.instance where instance_type='hospital'");
											$hitungData = pg_fetch_array($hitung);
											$banyakData = $hitungData[0];
											$halaman = isset($_GET['halaman']) ? $_GET['halaman'] : 1;
											$limit = 10;
											$mulai_dari = $limit * ($halaman - 1);
											$sql_limit = "select * from sinodar.instance where sinodar.instance.instance_type='hospital' order by id limit $limit offset $mulai_dari";
											$hasil = pg_query($sql_limit);
											$result = pg_query("select * from sinodar.instance where sinodar.instance.instance_type='hospital' order by id limit ");
											while($arr = pg_fetch_array($hasil)){
											?>
											<tr>
											<td><?php echo $arr['id'] ?></td>
											<td><?php echo $arr['name'] ?></td>
											<td><a href="bacainstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/magnifier.png" alt="Read" width="25" height="25" title="Read"></a><a href="ubahinstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/pencil.png" alt="Edit" width="25" height="25" style="margin-left:5px;" title="Edit"></a><a href="hapusinstansi.php?id=<?php echo $arr['id'] ?>" onclick="return confirm('Are you sure want to delete <?php echo $arr['name'] ?>')"><img border="0" src="../asset/icon/cross.png" alt="Delete" width="25" height="25" style="margin-left:5px; margin-right:5px" title="Delete"></a></td>
											</tr>
											<?php 
											}
											?>
											<?php
											$banyakHalaman = ceil($banyakData / $limit);
											echo 'Halaman: ';
											for($i = 1; $i <= $banyakHalaman; $i++){
 											if($halaman != $i){
 											echo '[<a href="instansi.php?halaman='.$i.'#tabs-1">'.$i.'</a>] ';
 											}else{
 											echo "[$i]  ";
 												}
											}
											?>
										</table>
										</div>
										<div id="tabs-2">
										<table class="table table-bordered">
											<tr>
												<th>ID</th>
												<th>Instance Name</th>
												<th width="90px">Action</th>
											</tr>
											<?php
											$hitung = pg_query("select count(id) from sinodar.instance where instance_type='police station'");
											$hitungData = pg_fetch_array($hitung);
											$banyakData = $hitungData[0];
											$page = isset($_GET['page']) ? $_GET['page'] : 1;
											$limit = 10;
											$mulai_dari = $limit * ($page - 1);
											$sql_limit = "select * from sinodar.instance where sinodar.instance.instance_type='police station' order by id limit $limit offset $mulai_dari";
											$hasil = pg_query($sql_limit);
											$result = pg_query("select * from sinodar.instance where sinodar.instance.instance_type='police station' order by id");
											while($arr = pg_fetch_array($hasil)){
											?>
											<tr>
											<td><?php echo $arr['id'] ?></td>
											<td><?php echo $arr['name'] ?></td>
											<td><a href="bacainstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/magnifier.png" alt="Read" width="25" height="25" title="Read"></a><a href="ubahinstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/pencil.png" alt="Edit" width="25" height="25" style="margin-left:5px;" title="Edit"></a><a href="hapusinstansi.php?id=<?php echo $arr['name'] ?>" onclick="return confirm('Are you sure want to delete <?php echo $arr['id'] ?>')"><img border="0" src="../asset/icon/cross.png" alt="Delete" width="25" height="25" style="margin-left:5px; margin-right:5px" title="Delete"></a></td>
											</tr>
											<?php 
											}
											?>
											<?php
											$banyakHalaman = ceil($banyakData / $limit);
											echo 'Halaman: ';
											for($i = 1; $i <= $banyakHalaman; $i++){
 											if($page != $i){
 											echo '[<a href="instansi.php?page='.$i.'#tabs-2">'.$i.'</a>] ';
 											}else{
 											echo "[$i]  ";
 												}
											}
											?>	
										</table>
										</div>
										<div id="tabs-3">
										<table class="table table-bordered">
											<tr>
												<th>ID</th>
												<th>Instance Name</th>
												<th width="90px">Action</th>
											</tr>
											<?php
											$hitung = pg_query("select count(id) from sinodar.instance where instance_type='fire station'");
											$hitungData = pg_fetch_array($hitung);
											$banyakData = $hitungData[0];
											$page = isset($_GET['page']) ? $_GET['page'] : 1;
											$limit = 10;
											$mulai_dari = $limit * ($page - 1);
											$sql_limit = "select * from sinodar.instance where sinodar.instance.instance_type='fire station' order by id limit $limit offset $mulai_dari";
											$hasil = pg_query($sql_limit);
											$result = pg_query("select * from sinodar.instance where sinodar.instance.instance_type='fire station' order by id");
											while($arr = pg_fetch_array($hasil)){
											?>
											<tr>
											<td><?php echo $arr['id'] ?></td>
											<td><?php echo $arr['name'] ?></td>
											<td><a href="bacainstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/magnifier.png" alt="Read" width="25" height="25" title="Read"></a><a href="ubahinstansi.php?id=<?php echo $arr['id'] ?>"><img border="0" src="../asset/icon/pencil.png" alt="Edit" width="25" height="25" style="margin-left:5px;" title="Edit"></a><a href="hapusinstansi.php?id=<?php echo $arr['id'] ?>" onclick="return confirm('Are you sure want to delete <?php echo $arr['name'] ?>')"><img border="0" src="../asset/icon/cross.png" alt="Delete" width="25" height="25" style="margin-left:5px; margin-right:5px" title="Delete"></a></td>
											</tr>
											<?php 
											}
											?>
											<?php
											$banyakHalaman = ceil($banyakData / $limit);
											echo 'Halaman: ';
											for($i = 1; $i <= $banyakHalaman; $i++){
 											if($page != $i){
 											echo '[<a href="instansi.php?page='.$i.'#tabs-3">'.$i.'</a>] ';
 											}else{
 											echo "[$i]  ";
 												}
											}
											?>
										</table>
										</div>
										</div>
							</div>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>