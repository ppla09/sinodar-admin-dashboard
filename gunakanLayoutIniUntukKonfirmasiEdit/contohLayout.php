<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/resize_height2.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
	</head>
	<body onload="resize_height(); resize_height2();" onresize="resize_height(); resize_height2();">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:2px; margin-left:10px;">
				<div style="width:570px; float:left; min-height:1px;">
					<div class="content-box">
						<div style="margin-left:20px; overflow:auto;">
						<div id="bar2">
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<!-- Kode untuk tampilkan form lama di sini -->
							<h2>Create new Entry</h2>
							<form action="" method="post" enctype="multipart/form-data">
							<div style="margin-left:40px">
								<div class="row" style="margin-top:15px">
									<label><b>Instance Name *</b></label>
									<input type="text" name="name" placeholder="Instance Name" required>
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Category *</b></label>
									<!-- Kalo pake dropdown menu mungkin lebih bagus? -->
									<input type="radio" name="instance_type" value="hospital" checked> Hospital
									<input type="radio" name="instance_type" value="police station" style="margin-left:10px;"> Police station
									<input type="radio" name="instance_type" value="fire station" style="margin-left:10px;"> Fire station
								</div>
								<div class="row" style="margin-top:20px">
									<label><b>Address *</b></label>
									<input type="text" name="address" placeholder="Address" pattern="[A-Za-z0-9\s-\/,/.]*" required>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Longitude *</b></label>
									<input type="text" id="longitude" name="longitude" placeholder="Longitude">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Latitude *</b></label>
									<input type="text" id="latitude" name="latitude" placeholder="Latitude">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Location</b></label>
									<div id="map_canvas"></div>
									<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = -6.389236;
										var defaultLng = 106.829363;

											// creates the map
											// zooms
											// centers the map
											// sets the map’s type
											var map = new google.maps.Map(document.getElementById('map_canvas'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});
											 
											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											 
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
											 
											// centers the map on markers coords
											map.setCenter(myMarker.position);
											 
											// adds the marker on the map
											myMarker.setMap(map);
									</script>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Phone *</b></label>
									<input type="text" name="primary_phone_num" placeholder="Phone number" required>
									<button class="btn">Other phone number</button>
								</div>
								<div class="row" style="margin-top:15px">
									<!-- Bisa pake textarea gak ya? -->
									<label><b>Description</b></label>
									<textarea rows="4" cols="50" name="description" placeholder="Description"></textarea>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Image</b></label>
									<input type="file" name="file">
								</div>
								<div class="row" style="margin-top:15px" align="center">
									<input class="btn" type="submit" name="submit" value="Submit">
								</div>
							</div>
							</form>
							<?php
								$picDir = '../../instance_pics/';
								$maxAllowedSize = 5 * 1024 * 1024; // in byte
								$allowedExts = array("gif", "jpeg", "jpg", "png");
								$allowedType = array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png");
								$extension = end(explode(".", $_FILES["file"]["name"]));
								if(in_array($_FILES["file"]["type"], $allowedType) && 
	  							 in_array($extension, $allowedExts) &&
	  							 $_FILES["file"]["size"] <= $maxAllowedSize) {
									if($_FILES["file"]["error"] > 0) {
										echo "<h1>Error: " . $_FILES["file"]["error"] . "</h1>";
										} else {
										$upFile = $picDir . date('Y_m_d_H_i_s') . $_FILES['file']['name'];
										if(!move_uploaded_file($_FILES["file"]["tmp_name"], $upFile)) {
											echo 'Problem could not move file to destination. Please check again later.';
											exit;
										}
								}
								} else {
								echo "<h3> </h3>";
								}
								if(isset($_POST['submit'])){
								$hasil = pg_query("insert into sinodar.instance(name,address,longitude,latitude,primary_phone_num,instance_type,description,picture_url,last_edit_timestamp) values('".$_POST['name']."','".$_POST['address']."','".$_POST['longitude']."','".$_POST['latitude']."','".$_POST['primary_phone_num']."','".$_POST['instance_type']."','".$_POST['description']."','$upFile',now())");
								if($hasil){
									echo "<script type='text/javascript'>alert('New entry successfully created');window.location.assign('instansi.php');</script>";
								} else{
									echo "<hr><br/><b>Failed to connect server. Please check your internet connection.</b>";
								}
								}
							?>
						</div>
						</div>
					</div>
				</div>
				<div style="border-left: thick solid black; width:570px; float:left; min-height:1px;">
					<div class="content-box">
						<div style="margin-left:20px; overflow:auto;">
						<div id="bar" style="margin-right:20px;">
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<!-- Kode untuk tampilkan form baru di sini -->
							<h2>Create new Entry</h2>
							<form action="" method="post" enctype="multipart/form-data">
							<div style="margin-left:40px">
								<div class="row" style="margin-top:15px">
									<label><b>Instance Name *</b></label>
									<input type="text" name="name" placeholder="Instance Name" required>
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Category *</b></label>
									<!-- Kalo pake dropdown menu mungkin lebih bagus? -->
									<input type="radio" name="instance_type" value="hospital" checked> Hospital
									<input type="radio" name="instance_type" value="police station" style="margin-left:10px;"> Police station
									<input type="radio" name="instance_type" value="fire station" style="margin-left:10px;"> Fire station
								</div>
								<div class="row" style="margin-top:20px">
									<label><b>Address *</b></label>
									<input type="text" name="address" placeholder="Address" pattern="[A-Za-z0-9\s-\/,/.]*" required>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Longitude *</b></label>
									<input type="text" id="longitude" name="longitude" placeholder="Longitude">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Latitude *</b></label>
									<input type="text" id="latitude" name="latitude" placeholder="Latitude">
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Location</b></label>
									<div id="map_canvas"></div>
									<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = -6.389236;
										var defaultLng = 106.829363;

											// creates the map
											// zooms
											// centers the map
											// sets the map’s type
											var map = new google.maps.Map(document.getElementById('map_canvas'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});
											 
											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											 
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
											 
											// centers the map on markers coords
											map.setCenter(myMarker.position);
											 
											// adds the marker on the map
											myMarker.setMap(map);
									</script>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Phone *</b></label>
									<input type="text" name="primary_phone_num" placeholder="Phone number" required>
									<button class="btn">Other phone number</button>
								</div>
								<div class="row" style="margin-top:15px">
									<!-- Bisa pake textarea gak ya? -->
									<label><b>Description</b></label>
									<textarea rows="4" cols="50" name="description" placeholder="Description"></textarea>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Image</b></label>
									<input type="file" name="file">
								</div>
								<div class="row" style="margin-top:15px" align="center">
									<input class="btn" type="submit" name="submit" value="Submit">
								</div>
							</div>
							</form>
							<?php
								$picDir = '../../instance_pics/';
								$maxAllowedSize = 5 * 1024 * 1024; // in byte
								$allowedExts = array("gif", "jpeg", "jpg", "png");
								$allowedType = array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png");
								$extension = end(explode(".", $_FILES["file"]["name"]));
								if(in_array($_FILES["file"]["type"], $allowedType) && 
	  							 in_array($extension, $allowedExts) &&
	  							 $_FILES["file"]["size"] <= $maxAllowedSize) {
									if($_FILES["file"]["error"] > 0) {
										echo "<h1>Error: " . $_FILES["file"]["error"] . "</h1>";
										} else {
										$upFile = $picDir . date('Y_m_d_H_i_s') . $_FILES['file']['name'];
										if(!move_uploaded_file($_FILES["file"]["tmp_name"], $upFile)) {
											echo 'Problem could not move file to destination. Please check again later.';
											exit;
										}
								}
								} else {
								echo "<h3> </h3>";
								}
								if(isset($_POST['submit'])){
								$hasil = pg_query("insert into sinodar.instance(name,address,longitude,latitude,primary_phone_num,instance_type,description,picture_url,last_edit_timestamp) values('".$_POST['name']."','".$_POST['address']."','".$_POST['longitude']."','".$_POST['latitude']."','".$_POST['primary_phone_num']."','".$_POST['instance_type']."','".$_POST['description']."','$upFile',now())");
								if($hasil){
									echo "<script type='text/javascript'>alert('New entry successfully created');window.location.assign('instansi.php');</script>";
								} else{
									echo "<hr><br/><b>Failed to connect server. Please check your internet connection.</b>";
								}
								}
							?>
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			</div>
		</div>
	</div>
	</body>
</html>