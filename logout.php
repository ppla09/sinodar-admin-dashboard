<?php
	session_start();
	
	if(!isset($_SESSION['username'])) {
		unset($_SESSION['error_msg']);
	} else {
		unset($_SESSION['error_msg']);
		unset($_SESSION['username']);
	}
	
	header('location: login.php');
?>