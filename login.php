<?php	
	include('../config.inc');

	session_start();
	
	if(isset($_SESSION['username'])) {
		header('location: home.php');
	}
	
	if(isset($_SESSION['error_msg'])) {
		$error[] = $_SESSION['error_msg'];
		unset($_SESSION['error_msg']);
	}
	
	if(isset($_POST['username']) && isset($_POST['password'])) {
		$result = pg_query("SELECT * FROM SINODAR.USER WHERE username = '" . $_POST['username'] . "' AND password = '" . md5($_POST['password']) . "';");
		$isAdmin = pg_query("SELECT * FROM SINODAR.ADMIN WHERE username = '" . $_POST['username'] . "';");
		
		if(pg_num_rows($result) && pg_num_rows($isAdmin)) {
			$_SESSION['username'] = $_POST['username'];
			header('location: home.php');
		} else {
			$error[] = "Wrong username or password.";
		}
	} 
?>

<!DOCTYPE html>
<html>
   <head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
   </head>
   <body class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:20px; margin-bottom:20px;" align="center">
				<div class="span3" align="right" style="margin-left:72px;">
					<img src="asset/logo/logo.png" alt="SiNoDar icon" height="150" width="150">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:63px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:50px;">
				<div class="span4">
				</div>
				<div class="span3 login-box" align="center">
				<form method="post" action="login.php">
						
					<h2>Login</h2> 
					<div class="row" style="margin-left:28px;">
					<label align="left"><b>Username</b><br><input name="username" type="text" required></label><br>
					</div>
					<div class="row" style="margin-left:28px;">
					<label align="left"><b>Password</b><br><input name="password" type="password" required></label><br>
					</div>
					<div class="row">
					<button type="submit" class="btn">Login</button>
					</div>
				</form>
					
				<?php
					for($i = 0; $i < count($error); $i++) {
						print("<p style = 'color: red;'> <strong>$error[$i]</strong> </p>");
					}
				?>
				</div>
				<div class="span4">
				</div>
			</div>
		</div>
	</body>
</html>