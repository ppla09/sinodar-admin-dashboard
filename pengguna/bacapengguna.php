<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="../instansi/instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasiedit/konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<?php
							$haha = "select * from sinodar.user where username='".$_GET['username']."'";
							$result = pg_query($haha);					
							while($arr = pg_fetch_array($result)){
							?>
							
							<h3><b><?php echo $arr['username'] ?></b></h3>
							<table class="table table-bordered">
							<tr>
								<th><b>Firstname</b></th>
								<th><?php echo $arr['firstname'] ?></th>
							</tr>
							<tr>	
								<th><b>Lastname</b></th>
								<th><?php echo $arr['lastname'] ?></th>
							</tr>
							<tr>	
								<th><b>E-mail</b></th>
								<th><?php echo $arr['email'] ?></th>	
							</tr>
							<?php
							}
							?>
							</table>
							<a href ="pengguna.php" class="btn btn-block" role="button" style="text-align:center; margin-bottom:20px;"><b>Back</b></a>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>