<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="../instansi/instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasiedit/konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<h2>Edit User</h2>
							<form action="" method="post">
								<?php
								$username = $_GET['username'];
								$result = pg_query("select * from sinodar.user where username='$username'");
								$arr = pg_fetch_array($result);
								if(isset($_POST['submit'])){
								$hasil = pg_query("update sinodar.user set firstname='".$_POST['firstname']."', lastname='".$_POST['lastname']."', email='".$_POST['email']."', password='".md5($_POST['password'])."' where username='".$_POST['username']."'");
								if($hasil){
									echo "<script type='text/javascript'>alert('User information successfully edited');window.location.assign('pengguna.php');</script>";
								} else{
									echo "<script type='text/javascript'>alert('User information failed to edited');window.location.assign('pengguna.php');</script>";
								}
								}?>
									<input type="hidden" name="username" value="<?php echo $arr['username'] ?>">
							<!-- Username user ga di tampilin? -->
							<div style="margin-left:40px">
								<div class="row" style="margin-top:15px">
									<label><b>Username</b></label>
									<input type="text" placeholder="<?php echo $arr ['username'] ?>" name = "username" disabled> 
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Password *</b></label>
									<input type="password" placeholder="Password" name="password" pattern="[A-Za-z0-9._]{6,100}" title ="Masukan dapat berupa huruf,angka,titik, dan garis bawah dengan panjang minimal 6 karakter dan maksimal 100 karakter" required>
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Firstname *</b></label>
									<input type="text" name="firstname" pattern="[A-Za-z]{5,100}" value="<?php echo $arr['firstname'] ?>" title ="Masukan dapat berupa huruf dengan panjang minimal 5 karakter dan maksimal 100 karakter" required>
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>Lastname</b></label>
									<input type="text" name="lastname" pattern="[A-Za-z]{0,100}" value="<?php echo $arr['lastname'] ?>" title ="Masukan dapat berupa huruf dengan panjang minimal 5 karakter dan maksimal 100 karakter">
								</div>
								<div class="row" style="margin-top:10px">
									<label><b>E-mail *</b></label>
									<input type="email" value="<?php echo $arr['email'] ?>" name="email" pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$" title ="Masukan dapat berupa seperti ini=a@a.com	"/></>
								</div>
								<div class="row" style="margin-top:10px" align="center">
									<input class="btn" type="submit" name="submit" value="Submit">
								</div>
							</div>
							</form>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>