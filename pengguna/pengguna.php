<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<link href="../css/jquery-ui-1.10.4.custom.css" rel="stylesheet">
		<script src="../js/jquery-1.10.2.js"></script>
		<script src="../js/jquery-ui-1.10.4.custom.js"></script>
		<script>$(function(){$("#tabs").tabs();});</script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="../instansi/instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasiedit/konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<div>
							<h2>User List</h2>
							</div>
							<div>
							<a class="btn" role="button" href="tambahpengguna.php">Add new user</a>
							</div>
							<!--<div style="margin-top:10px">
							<table class="table table-bordered">
							<tr>
								<th>Username</th>
								<th width="90px">Action</th>
							</tr>
							<?php
							if(!conn)
							{DIE('Connection is failed');}	
							$result = pg_query("select * from sinodar.user order by username");
							while($arr = pg_fetch_array($result)){
							?>
							<tr>
								<td><?php echo $arr['username'] ?></td>
								<td><a href="bacapengguna.php?username=<?php echo $arr['username'] ?>"><img border="0" src="../asset/icon/magnifier.png" alt="Read" width="25" height="25" title="Read"></a><a href="ubahpengguna.php?username=<?php echo $arr['username'] ?>"><img border="0" src="../asset/icon/pencil.png" alt="Edit" width="25" height="25" style="margin-left:5px;" title="Edit"></a><a href="hapuspengguna.php?username=<?php echo $arr['username'] ?>" onclick="return confirm('Are you sure want to delete <?php echo $arr['username'] ?>')"><img border="0" src="../asset/icon/cross.png" alt="Delete" width="25" height="25" style="margin-left:5px; margin-right:5px" title="Delete"></a></td>
							</tr>
							<?php 
							}
							?>
							</table>
							</div>-->
							<div style="margin-top:10px">
								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Pengguna</a></li>
									</ul>
										<div id="tabs-1">
										<table class="table table-bordered">
							<tr>
								<th>Username</th>
								<th width="90px">Action</th>
							</tr>
							<?php
							$hitung = pg_query("select count(username) from sinodar.user");
							$hitungData = pg_fetch_array($hitung);
							$banyakData = $hitungData[0];
							$halaman = isset($_GET['halaman']) ? $_GET['halaman'] : 1;
							$limit = 5;
							$mulai_dari = $limit * ($halaman - 1);
							$sql_limit = "select b.username from sinodar.user b where not exists(select * from sinodar.admin a where a.username=b.username and a.username='admin')  order by username limit $limit offset $mulai_dari";
							$hasil = pg_query($sql_limit);
							$result = pg_query("select * from sinodar.user order by username");
							while($arr = pg_fetch_array($hasil)){
							?>
							<tr>
								<td><?php echo $arr['username'] ?></td>
								<td><a href="bacapengguna.php?username=<?php echo $arr['username'] ?>"><img border="0" src="../asset/icon/magnifier.png" alt="Read" width="25" height="25" title="Read"></a><a href="ubahpengguna.php?username=<?php echo $arr['username'] ?>"><img border="0" src="../asset/icon/pencil.png" alt="Edit" width="25" height="25" style="margin-left:5px;" title="Edit"></a><a href="hapuspengguna.php?username=<?php echo $arr['username'] ?>" onclick="return confirm('Are you sure want to delete <?php echo $arr['username'] ?>')"><img border="0" src="../asset/icon/cross.png" alt="Delete" width="25" height="25" style="margin-left:5px; margin-right:5px" title="Delete"></a></td>
							</tr>
							<?php 
							}
							?>
							<?php
											$banyakHalaman = ceil($banyakData / $limit);
											echo 'Halaman: ';
											for($i = 1; $i <= $banyakHalaman; $i++){
 											if($halaman != $i){
 											echo '[<a href="pengguna.php?halaman='.$i.'#tabs-1">'.$i.'</a>] ';
 											}else{
 											echo "[$i]  ";
 												}
											}
											?>

							</table>
							</div>
							</div>
							</div>
	
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>