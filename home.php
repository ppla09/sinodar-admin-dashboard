<?php
	session_start();
	
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/resize_height.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<script>
		// Konfigurasi Google Maps
			var myZoom = 12;
			var myMarkerIsDraggable = true;
			var myCoordsLenght = 6;
			var defaultLat = -6.389236;
			var defaultLng = 106.829363;

			//Membuat Google Maps beserta propertinya
			var map = new google.maps.Map(document.getElementById('map_canvas'), {
				zoom: myZoom,
				center: new google.maps.LatLng(defaultLat, defaultLng),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});							 
			// Membuat marker yang bisa dipindah
			var myMarker = new google.maps.Marker({
				position: new google.maps.LatLng(defaultLat, defaultLng),
				draggable: myMarkerIsDraggable
			});
			// Menambahkan listener ke marker
			google.maps.event.addListener(myMarker, 'dragend', function(evt){
				document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
				document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
			});
			// Posisi marker ketika google maps diload pertama kali
			map.setCenter(myMarker.position);
			// Menambahkan marker pada map
			myMarker.setMap(map);
		</script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>
	</head>
	
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="instansi/instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="pengguna/pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="konfirmasiedit/konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
					<div id="bar" style="margin-left:20px; overflow:auto;">
						<h1>Welcome to SiNoDar Admin Dashboard.</h1>
					</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>