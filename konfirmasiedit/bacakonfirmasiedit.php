<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height3.js"></script>
		<script src="../js/resize_height4.js"></script>
		<script src="../js/konfigurasiGMAPs.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		#map_canvas1 {
        		width: 250px;
        		height: 250px;
      		}
		</style>
		
	</head>
	<body onload="resize_height3(); resize_height4();" onresize="resize_height3(); resize_height4();">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<form name = "form1" action="databaru.php" method="post">
			<div class="row" style="margin-top:2px; margin-left:10px;">
				<div style="width:570px; float:left; min-height:1px;">
					<div class="content-box">
						<div style="margin-left:20px; overflow:auto;">
						<div id="bar4">
							<?php
							$result = pg_query("select * from sinodar.instance where id='".$_GET['id']."'");
							while($arr = pg_fetch_array($result)){
							?>
							<?php
							$result2 = pg_query("select * from sinodar.suggest_edit where id='".$_GET['id']."' and username ='".$_GET['username']."' and edit_timestamp='".$_GET['edit_timestamp']."'");
							while($arr2 = pg_fetch_array($result2)){
							?>

							<h3><b><?php echo $arr['name'] ?></b></h3>

							<?php
							if( $arr2['name']!=null &&  $arr2['name']!="" && !empty( $arr2['name'])){
								echo '<input type="radio" name="nama" value="private" onclick="setName(1)" checked>Old Name</input>';
							}							
							?>
							</br>
							<table class="table table-bordered">
							
							<tr>	
								<th><b>ID</b></th>
								<th><?php echo $arr['id'] ?></th>
							</tr>
							
							<?php 
							if( $arr2['address']!=null &&  $arr2['address']!="" && !empty( $arr2['address'])){
							echo '<tr>';
								echo '<th><input type="radio" name="alamat" value="private" onclick="setAddress(1)" checked>Old Address</input></th>';
								echo '<th>';
								echo $arr['address'];
								echo '</th>';
							echo '</tr>';
							}
							?>							
							
							<?php
							if( ($arr2['latitude']!=null &&  $arr2['latitude']!="" && !empty( $arr2['latitude'])) &&
								($arr2['longitude']!=null &&  $arr2['longitude']!="" && !empty( $arr2['longitude'])) ){
							echo '<tr>';
								echo '<th><input type="radio" name="lokasi" value="private" onclick="setLocation(1)" checked>Old Location</input></th>';
								echo '<th><div id="map_canvas"></div></th>';
							echo '</tr>';
							}
							?>
								<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = <?php echo json_encode(rad2deg($arr['latitude'])); ?>;
										var defaultLng = <?php echo json_encode(rad2deg($arr['longitude'])); ?>;

											// creates the map
											// zooms
											// centers the map
											// sets the maps type
											var map = new google.maps.Map(document.getElementById('map_canvas'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});
											 
											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											 
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
											 
											// centers the map on markers coords
											map.setCenter(myMarker.position);
											 
											// adds the marker on the map
											myMarker.setMap(map);
									</script>
							<?php
							if( $arr2['primary_phone_num']!=null &&  $arr2['primary_phone_num']!="" && !empty( $arr2['primary_phone_num'])){
							echo '<tr>';
								echo '<th><input type="radio" name="telepon" value="private" onclick="setPhonenumber(1)" checked>Old Primary phone numer</input></th>';
								echo '<th>'; echo $arr['primary_phone_num']; echo '</th>';
							echo '</tr>';
							}
							?>
							
							

							<?php
							if( $arr2['description']!=null &&  $arr2['description']!="" && !empty( $arr2['description'])){
							echo '<tr>';
								echo '<th><input type="radio" name="deskripsi" value="private" onclick="setDescription(1)" checked>Old Description</input></th>';
								echo '<th>'; echo $arr['description']; echo '</th>';
							echo '</tr>';
							}
							?>
							
							<?php 
							if( $arr2['picture_url']!=null &&  $arr2['picture_url']!="" && !empty( $arr2['picture_url'])){
								echo '<tr>';
								echo '<th><input type="radio" name="gambar" value="private" onclick="setPicture(1)" checked>Old Picture</input></th>';
								echo '<th>'; echo '<img src='; echo $arr['picture_url']; echo '></img>'; 
								echo $arr['picture_url'];
								echo '</th>';
								echo '</tr>';	
							}
							?>
													
							
							<tr>
								<th><b>Last edited</b></th>
								<th><?php echo $arr['last_edit_timestamp'] ?></th>
							</tr>
							</table>
							<?php $resultNumber2 = pg_query("select phone_num from sinodar.suggest_edit_other_number where id='".$_GET['id']."' and username ='".$_GET['username']."' and edit_timestamp='".$_GET['edit_timestamp']."'");
							 
							if(pg_num_rows($resultNumber2)>0){
							echo '<label><b>Other Phone number</b></label>';
								$resultNumber = pg_query("select phone_num from sinodar.instance_other_number where id='".$arr['id']."'");
								$counter=0;
								$n = pg_fetch_array($resultNumber);
								if(pg_num_rows($resultNumber)>0){
								while($nums = pg_fetch_array($resultNumber)){
									$number = $nums['phone_num'];
									echo '<input type="checkbox" name="chkA[]'; 
									echo '" checked />';
									
									echo'<input type="text" name="otherNumA[]';
									echo '" value="';
									echo $number;
									echo '" readonly/>';
									$counter=counter+1;
								}
								} else { echo '<label>No other number</label>'; }
							
							}
							?>

							

							
						</div>
						</div>
					</div>
				</div>
				<div style="border-left: thick solid black; width:570px; float:left; min-height:1px;">
					<div class="content-box">
						<div style="margin-left:20px; overflow:auto;">
						<div id="bar3" style="margin-right:20px;">
							
							<?php
							if( $arr2['name']!=null &&  $arr2['name']!="" && !empty( $arr2['name'])){ 
							echo '<h3><b>'; echo $arr2['name']; echo '</b></h3>';
							echo '<input type="radio" name="nama" value="private" onclick="setName(2)">New Name</input>';
							}
							?>
							</br>
							<table class="table table-bordered">			
							<tr>
								<th><b>ID</b></th>
								<th><?php echo $arr2['id'] ?></th>
	
							</tr>
							<?php 
							if( $arr2['address']!=null &&  $arr2['address']!="" && !empty( $arr2['address'])){
							echo '<tr>';
								echo '<th><input type="radio" name="alamat" value="private" onclick="setAddress(2)">New Address</input></th>';
								echo '<th><b>Address</b></th>';
								echo '<th>';
								echo $arr2['address'];
								echo '</th>';
							echo '</tr>';
							}
							?>
							
							<?php
							if( ($arr2['latitude']!=null &&  $arr2['latitude']!="" && !empty( $arr2['latitude'])) &&
								($arr2['longitude']!=null &&  $arr2['longitude']!="" && !empty( $arr2['longitude'])) ){
							echo '<tr>';
								echo '<th><input type="radio" name="lokasi" value="private" onclick="setLocation(2)">New Location</input></th>';
								echo '<th><b>Posisi</b></th>';
								echo '<th><div id="map_canvas"></div></th>';
							echo '</tr>';
							}
							?>

								<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = <?php echo json_encode(rad2deg($arr2['latitude'])); ?>;
										var defaultLng = <?php echo json_encode(rad2deg($arr2['longitude'])); ?>;

											// creates the map
											// zooms
											// centers the map
											// sets the maps type
											var map = new google.maps.Map(document.getElementById('map_canvas1'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});
											 
											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											 
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
											 
											// centers the map on markers coords
											map.setCenter(myMarker.position);
											 
											// adds the marker on the map
											myMarker.setMap(map);
									</script>
							<?php
							if( $arr2['primary_phone_num']!=null &&  $arr2['primary_phone_num']!="" && !empty( $arr2['primary_phone_num'])){
							echo '<tr>';
								echo '<th><input type="radio" name="telepon" value="private" onclick="setPhonenumber(2)">New Primary phone numer</input></th>';
								echo '<th>'; echo $arr2['primary_phone_num']; echo '</th>';
							echo '</tr>';
							}
							?>
												
							<?php
							if( $arr2['description']!=null &&  $arr2['description']!="" && !empty( $arr2['description'])){
							echo '<tr>';
								echo '<th><input type="radio" name="deskripsi" value="private" onclick="setDescription(2)">New Description</input></th>';
								echo '<th>'; echo $arr2['description']; echo '</th>';
							echo '</tr>';
							}
							?>
							
							<?php 
							if( $arr2['picture_url']!=null &&  $arr2['picture_url']!="" && !empty( $arr2['picture_url'])){
								echo '<tr>';
								echo '<th><input type="radio" name="gambar" value="private" onclick="setPicture(2)">New Picture</input></th>';
								echo '<th>'; echo '<img src='; echo $arr2['picture_url']; echo '></img>'; 
								echo $arr2['picture_url'];
								echo '</th>';
								echo '</tr>';	
							}
							?>
								
							<tr>
								<th><b>Last edited</b></th>
								<th><?php echo $arr2['edit_timestamp'] ?></th>
							</tr>
							</table>
							<?php $resultNumber2 = pg_query("select phone_num from sinodar.suggest_edit_other_number where id='".$_GET['id']."' and username ='".$_GET['username']."' and edit_timestamp='".$_GET['edit_timestamp']."'");
							
							if(pg_num_rows($resultNumber2)>0){
							echo '<label><b>Other Phone number</b></label>';
								$counter=0;
								if(pg_num_rows($resultNumber2)>0){
									while($nums = pg_fetch_array($resultNumber2)){
										$number = $nums['phone_num'];
										echo '<input type="checkbox" name="chkB[]';
										echo '" />';
									
										echo'<input type="text" name="otherNumB[]';	
										echo '" value="';
										echo $number;
										echo '" readonly/>';
										$counter=counter+1;
										echo '<br>';
									}
								}
							
							}
							?>

								
							
							
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="row" style="margin-top:15px" align="center">
				
				<script>
					function setName(e){
						t=document.getElementsByName('upName')[0];
						if(e==1){
    							t.value = '<?php echo $arr['name']; ?>';
						}
						else if(e==2){
							x = '<?php echo $arr2['name']; ?>';
							if(x!=null || !empty(x) || x!=""){
    								t.value = x ;
							}else{
								t.value = '<?php echo $arr['name']; ?>';
							}
						}
    					}
					
					function setAddress(e){
						t=document.getElementsByName('upAddress')[0];
						if(e==1){
    							t.value = '<?php echo $arr['address']; ?>';
						}
						else if(e==2){
							x = '<?php echo $arr2['address']; ?>';
							if(x!=null || !empty(x) || x!=""){
    								t.value = x ;
							}else{
								t.value = '<?php echo $arr['address']; ?>';
							}
						}
    					}

					function setLocation(e){
						t=document.getElementsByName('upLatitude')[0];
						u=document.getElementsByName('upLongitude')[0];
						if(e==1){
    							t.value = '<?php echo $arr['latitude']; ?>';
							u.value = '<?php echo $arr['longitude']; ?>';
						}
						else if(e==2){
							x = '<?php echo $arr2['latitude']; ?>';
							y = '<?php echo $arr2['longitude']; ?>';
							if((x!=null || !empty(x) || x!="") && (y!=null || !empty(y) || y!="")){
    								t.value = x ;
								u.value = y ;
							}else{
								t.value = '<?php echo $arr['latitude']; ?>';
								u.value = '<?php echo $arr['longitude']; ?>';
							}
						}
    					}

					function setPhonenumber(e){
						t=document.getElementsByName('upPhonenumber')[0];
						if(e==1){
    							t.value = '<?php echo $arr['primary_phone_num']; ?>';
						}
						else if(e==2){
							x = '<?php echo $arr2['primary_phone_num']; ?>';
							if(x!=null || !empty(x) || x!=""){
    								t.value = x ;
							}else{
								t.value = '<?php echo $arr['primary_phone_num']; ?>';
							}
						}
    					}

					function setDescription(e){
						t=document.getElementsByName('upDescription')[0];
						if(e==1){
    							t.value = '<?php echo $arr['description']; ?>';
						}
						else if(e==2){
							x = '<?php echo $arr2['description']; ?>';
							if(x!=null || !empty(x) || x!=""){
    								t.value = x ;
							}else{
								t.value = '<?php echo $arr['description']; ?>';
							}
						}
    					}
					
					function setPicture(e){
						t=document.getElementsByName('upPicture')[0];
						u=document.getElementsByName('new_picture')[0];
						if(e==1){
    							t.value = '<?php echo $arr['picture_url']; ?>';
							u.value = 0;
						}
						else if(e==2){
							x = '<?php echo $arr2['picture_url']; ?>';
							if(x!=null || !empty(x) || x!=""){
    								t.value = x ; u.value = 1;
							}else{
								t.value = '<?php echo $arr['picture_url']; ?>';
							}
						}
    					}

				</script>
				
				
					<!-- Dibawah ini di set Visible untuk debugging, kalo udh jadi bakal di set type hidden -->
					<input type="hidden" name="upId" placeholder="upId" value="<?php echo $arr['id']; ?>" readonly/>
					<input type="hidden" name="upName" placeholder="upName" value="<?php echo $arr['name']; ?>" />
					<input type="hidden" name="upAddress" placeholder="upAddress" value="<?php echo $arr['address']; ?>" />
					<input type="hidden" name="upLatitude" placeholder="upLatitude" value="<?php echo $arr['latitude']; ?>" />
					<input type="hidden" name="upLongitude" placeholder="upLongitude" value="<?php echo $arr['longitude']; ?>" />
					<input type="hidden" name="upPhonenumber" placeholder="upPhonenumber" value="<?php echo $arr['primary_phone_num']; ?>" />
					<!--<input type="textbox" name="upOthernumber" placeholder="upOthernumber" />-->
					<input type="hidden" rows="4" cols="50" name="upDescription" placeholder="upDescription" value="<?php echo $arr['description']; ?>" />
					<input type="hidden" name="upPicture" placeholder="upPicture" value="<?php echo $arr['picture_url']; ?>" />
					<input type="hidden" name="username" value="<?php echo $arr2['username']; ?>" />
					<input type="hidden" name="edit_timestamp" value="<?php echo $arr2['edit_timestamp']; ?>" />
					<input type="hidden" name="new_picture" value=0 />
					</br>
					<input class="btn" type="submit" name="submit" value="Preview" />
					<a href="konfirmasiedit.php" class="btn">Back</a>
					
				</form>
				<?php
				}
				?>
				<?php
				}
				?>
			</div>
			</div>
			</div>
	</div>
	</body>
</html>