<?php
	session_start();
	include('../../config.inc');
	if(!isset($_SESSION['username'])) {
		$_SESSION['error_msg'] = 'Anda belum login!';
		header('location: ../login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>SiNoDar Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="../asset/logo/icon.png" />
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/resize_height.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    		<style>
		#map_canvas {
        		width: 250px;
        		height: 250px;
      		}
		</style>	</head>
	<body onload="resize_height()" onresize="resize_height()">
	<div class=" bg_pages">
		<div class="container">
			<div class="row" style="margin-top:10px; margin-bottom:10px;" align="center">
				<div class="span3" align="right" style="margin-left:70px;">
				<img src="../asset/logo/logo.png" alt="SiNoDar icon" height="100" width="100">
				</div>
				<div class="span8" style="margin-left:8px" align="left">
				<div style="margin-top:40px;">
					<font size="10"><b>SiNoDar Admin Dashboard</b></font>
				</div>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row">
				<div class="span9" style="margin-left:18px;">
					<?php
						echo "<h3>Hello, " . $_SESSION['username'] . " keep up the good work!</h3>";
					?>
				</div>
				<div class="span2" align="right">
					Don't forget to, <a href="../logout.php" onclick="return confirm('Are you sure want to logout?')"><u><b>Logout</b></u></a>
				</div>
			</div>
			<div class="row" style="background-color:black; height:3px">
			</div>
			<div class="row" style="margin-top:10px; margin-left:0px;">
				<div class="span2">
					<div class="row">
						<a href="../instansi/instansi.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Instance list</b>
						</div>
						</a>	
					</div>
					<div class="row">
						<a href="../pengguna/pengguna.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>User list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="../konfirmasibaru/konfirmasibaru.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>New entry suggestion list</b>
						</div>
						</a>
					</div>
					<div class="row">
						<a href="konfirmasiedit.php" class="btn btn-block" role="button" style="height:40px;">
						<div style="margin-top:4px;margin-left:5px;">
						<b>Information edit suggestion list</b>
						</div>
						</a>
					</div>
				</div>
				<div class="span9" style="border-left: thick solid black;">
					<div class="content-box">
					<div class="bg_content">
						<div id="bar" style="margin-left:20px; overflow:auto;">
						<div style="margin-right:20px;">
							<form action="" method="post" enctype="multipart/form-data">
							<div style="margin-left:40px">
								<?php
								$id = $_GET['id'];
								$result = pg_query("select * from sinodar.instance where id='$id'");
								$arr = pg_fetch_array($result);?>
									<input type="hidden" name="id" value="<?php echo $arr['id'] ?>">
								<div class="row" style="margin-top:15px">
									<label><b>Instance Name *</b></label>
									<input type="text" name="name" value="<?php echo $arr['name'] ?>" disabled>
								</div>
								<div class="row" style="margin-top:20px">
									<label><b>Address *</b></label>
									<input type="text" name="address" value="<?php echo $arr['address'] ?>" disabled>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Location</b></label>
									<div id="map_canvas"></div>
									<script>
								  	// configuration
										var myZoom = 12;
										var myMarkerIsDraggable = true;
										var myCoordsLenght = 6;
										var defaultLat = <?php echo json_encode(rad2deg($arr['latitude'])); ?>;
										var defaultLng = <?php echo json_encode(rad2deg($arr['longitude'])); ?>;

											// creates the map
											// zooms
											// centers the map
											// sets the map�s type
											var map = new google.maps.Map(document.getElementById('map_canvas'), {
												zoom: myZoom,
												center: new google.maps.LatLng(defaultLat, defaultLng),
												mapTypeId: google.maps.MapTypeId.ROADMAP
											});
											 
											// creates a draggable marker to the given coords
											var myMarker = new google.maps.Marker({
												position: new google.maps.LatLng(defaultLat, defaultLng),
												draggable: myMarkerIsDraggable
											});
											 
											// adds a listener to the marker
											// gets the coords when drag event ends
											// then updates the input with the new coords
											google.maps.event.addListener(myMarker, 'dragend', function(evt){
												document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
												document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
											});
											 
											// centers the map on markers coords
											map.setCenter(myMarker.position);
											 
											// adds the marker on the map
											myMarker.setMap(map);
									</script>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Phone *</b></label>
									<input type="text" name="primary_phone_num" value="<?php echo $arr['primary_phone_num'] ?>" disabled>
								</div>
								<div class="row" style="margin-top:15px">
									<label><b>Description</b></label>
									<label><?php echo $arr['description'] ?></label>
									</div>
								<div class="row" style="margin-top:15px">
									<label><b>Image</b></label>
									<img src="<?php echo $arr['picture_url'] ?>" width=200 height=150></img>
								</div>
								<div class="row" style="margin-top:15px" align="center">
									<input class="btn" type="submit" name="submit" value="Submit">
									<a href="konfirmasiedit.php" class="btn">Back</a>
								</div>
							</div>
							</form>
								<?php
								if(isset($_POST['submit'])){
								$latitude = deg2rad($_POST['latitude']);
								$longitude = deg2rad($_POST['longitude']);
								$hasil = pg_query("update sinodar.instance set name='".$_POST['name']."', address='".$_POST['address']."', longitude='$longitude', latitude='$latitude', primary_phone_num='".$_POST['primary_phone_num']."', description='".$_POST['description']."', picture_url='$upFile', last_edit_timestamp=now() where id='".$_POST['id']."'");
								if($hasil){	
										echo "<script type='text/javascript'>alert('Instance information successfully edited');window.location.assign('konfirmasiedit.php');</script>";

									} else{
										echo $hasil;
									}
								}
							?>

						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>