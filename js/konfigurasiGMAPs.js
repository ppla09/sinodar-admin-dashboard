
// Konfigurasi Google Maps
var myZoom = 12;
var myMarkerIsDraggable = true;
var myCoordsLenght = 6;
var defaultLat = -6.389236;
var defaultLng = 106.829363;

//Membuat Google Maps beserta propertinya
var map = new google.maps.Map(document.getElementById('map_canvas'), {
	zoom: myZoom,
	center: new google.maps.LatLng(defaultLat, defaultLng),
	mapTypeId: google.maps.MapTypeId.ROADMAP
});							 
// Membuat marker yang bisa dipindah
var myMarker = new google.maps.Marker({
	position: new google.maps.LatLng(defaultLat, defaultLng),
	draggable: myMarkerIsDraggable
});
// Menambahkan listener ke marker
google.maps.event.addListener(myMarker, 'dragend', function(evt){
	document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
	document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
});
// Posisi marker ketika google maps diload pertama kali
map.setCenter(myMarker.position);
// Menambahkan marker pada map
myMarker.setMap(map);